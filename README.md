# POC Unit testing

At this state, this is not "**the ideal template/structure**", but can be a basis for the rest of our projects.

We achieved to build with Conan and Cmake in C++, using gtest.

If needed, here is an example of the profile use :

```

[settings]
os=Windows
os_build=Windows
arch=x86_64
arch_build=x86_64
build_type=Debug

compiler=Visual Studio
compiler.runtime=MDd
compiler.version=16

[options]
[build_requires]
[env]
```


## Getting further

### Theorical/Generic

* getting started CMake : <https://cmake.org/cmake/help/v3.21/>
* doc of CMake : <https://google.github.io/googletest/quickstart-cmake.html>
* CMake tutorial (video) : <https://www.youtube.com/watch?v=nlKcXPUJGwA&list=PLalVdRk2RC6o5GHu618ARWh0VO0bFlif4&index=1>
* learn conan : <https://docs.conan.io/en/latest/training_courses.html>
* learn conan (video) : <https://academy.jfrog.com/conan-essentials>
* use gtest in cpp : <https://approvaltestscpp.readthedocs.io/en/latest/generated_docs/UsingGoogleTests.html>

### Practical

* setup Conan/Clion from Guillaume : <https://docs.google.com/document/d/1fwBjN6pRaKLnMu91jmqjFOKnaeouhc1KsXxG61aMXls/edit>
* gtest in Clion : <https://www.jetbrains.com/help/clion/creating-google-test-run-debug-configuration-for-test.html#add-google-tests>
* gtest in Clion (video) : <https://www.youtube.com/watch?v=8Up5eNZ0FLw>